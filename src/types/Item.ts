export type BasicItem = {
  id: string,
  name?: string
};

export type TextItem = BasicItem & {
  text: string
};

export type LocalImageItem = BasicItem & {
  image: File,
  alt: string,
  src?: string,
  toJSON: () => RemoteImageItem
};

export type RemoteImageItem = BasicItem & {
  src: string,
  alt: string,
  imageName: string
};

export type ImageItem = LocalImageItem | RemoteImageItem;

export type Item = TextItem | ImageItem;
