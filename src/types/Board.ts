import type {Item} from './Item';

export type Preferences = {
  columns: number,
  panelWidth: number,
  direction: 'rtl' | 'ltr'
}

export type Board = {
  id: string,
  preferences: Preferences,
  items: Item[]
};
