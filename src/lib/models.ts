import type {Board} from '../types/Board';
import type {Item, TextItem, LocalImageItem, RemoteImageItem, ImageItem} from '../types/Item';
import {id} from './id';

export function createTextItem(text = ''): TextItem {
  return {
    id: id(),
    text
  };
}
export function LocalImageToJSON(): RemoteImageItem {
  const {id, name, image, alt, src} = this as LocalImageItem;

  return {id, name, imageName: image.name, alt, src};
}

export function createLocalImageItem(image: File): LocalImageItem {
  return {
    id: id(),
    alt: image.name,
    image,
    toJSON: LocalImageToJSON
  };
}

export function isTextItem(item: Item): item is TextItem {
  return 'text' in item;
}

export function isLocalImageItem(item: Item): item is LocalImageItem {
  return 'image' in item;
}

export function isRemoteImageItem(item: Item): item is RemoteImageItem {
  return 'src' in item;
}

export function isImageItem(item: Item): item is ImageItem {
  return isLocalImageItem(item) || isRemoteImageItem(item);
}

export function createBoard(): Board {
  return {
    id: id(),
    preferences: {
      columns: 2,
      panelWidth: 300,
      direction: 'rtl'
    },
    items: []
  };
}

export function createInitialBoard(): Board {
  return {
    ...createBoard(),
    items: [createTextItem(), createTextItem(), createTextItem()]
  };
}

export function boardURL(board: Board) {
  return new URL(`?board=${board.id}`, location.href);
}
