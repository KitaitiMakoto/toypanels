import {readAndCompressImage} from 'browser-image-resizer';
import {createClient} from '@supabase/supabase-js';
import type {Board} from '../../types/Board';
import type {LocalImageItem, RemoteImageItem} from '../../types/Item';

const TABLE = 'boards';
const BUCKET = 'images';

const supabaseUrl = import.meta.env.VITE_SUPABASE_URL;
const supabaseAnonKey = import.meta.env.VITE_SUPABASE_ANON_KEY;

const supabase = createClient(supabaseUrl, supabaseAnonKey);

let CACHE: string;

function storagePath(item: Item): string {
  return item.id;
}

export async function fetchBoard(id: string): Promise<Board> {
  const result = await supabase
    .from(TABLE)
    .select('id, preferences, items')
    .eq('id', id)
    .single();

  console.debug('fetched', result.data);
  CACHE = JSON.stringify(result.data);

  return result.data;
}

export async function saveBoard(board: Board) {
  console.debug('saving', board);
  const newCache = JSON.stringify(board);
  if (newCache === CACHE) {
    console.debug('skip saving')
  }
  CACHE = newCache;

  return await supabase
    .from(TABLE)
    .upsert(board);
}

export async function saveImage(item: LocalImageItem) {
  console.debug('saving image', item);
  const image = await readAndCompressImage(item.image, {
    mimeType: item.image.type,
    maxWidth: 680,
    maxHeight: Infinity
  });
  const {data, error} = await supabase.storage
    .from(BUCKET)
    .upload(storagePath(item), image, {contentType: image.type});
  if (error) {
    console.error(error);
    throw error;
  } else {
    const {data: {publicUrl}} = supabase.storage
      .from(BUCKET)
      .getPublicUrl(data.path);
    return publicUrl;
  }
}

export async function removeImage(item: RemoteImageItem) {
  console.debug('removing image', item);
  const {error} = await supabase.storage
    .from(BUCKET)
    .remove([storagePath(item)]);
  if (error) {
    console.error(error);
  }
}
