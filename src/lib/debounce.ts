export function debounce(f: (args: any) => any, delay: number) {
  let timeoutID;
  return args => {
    clearTimeout(timeoutID);
    timeoutID = setTimeout(() => f(args), delay);
  };
}
